package application;
// Hautin Pierre etape 3 gestion des event via serveur
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main_javaFx_etape3 extends Application{

	@Override
	public void start(Stage primaryStage) {
		// Titre de l'appli
		
		//".setAlignment()" // permet de choisir alignement
		//".setHgap()" // permet de choisir l'espacement horizontal entre les "case"
		//".setVgap()" // permet de choisir l'espacement vertical entre les "case"
		//"TextField" // case de saisie
		//".add()" // permet d'ajouter
		//".getChildren().add()" // ajout un enfant
		//.setStroke() // pour le contour
		primaryStage.setTitle("Appli JavaFX Etape 3");

		GridPane root = new GridPane(); //GridPrincipal
		root.setGridLinesVisible(false); 
		root.setPadding(new Insets(1, 1, 1, 1));

		GridPane gridsaisie = new GridPane(); //Grid saisie setId=mefGridSaisie
		gridsaisie.setPadding(new Insets(1, 1, 1, 1));

		GridPane gridbtn = new GridPane(); // Grid btn setId=mefGridBtn
		gridbtn.setPadding(new Insets(1, 1, 1, 1));
		
		GridPane gridres = new GridPane (); // grid resultat setId=mefGridRes
		gridres.setPadding(new Insets(1, 1, 1, 1));
		
		//Calculatrice
		Label scenetitle = new Label("Calculatrice");
		root.add(scenetitle, 0,0);

		//nb 1
		Label nb1 = new Label("1er Nombre:");
		gridsaisie.add(nb1, 0, 0);
		TextField tfnb1 = new TextField( );
		gridsaisie.add(tfnb1, 1, 0);

		//nb 2
		Label nb2 = new Label("2eme Nombre:");
		gridsaisie.add(nb2, 0, 1);
		TextField tfnb2 = new TextField();
		gridsaisie.add(tfnb2, 1, 1);
		
		//Add
		Button btnadd = new Button("Additionner");
		HBox hbadd = new HBox(3);
		hbadd.getChildren().add(btnadd); 
		gridbtn.add(hbadd, 0, 0);

		//Sous
		Button btnsous = new Button("Soustrction");
		HBox hbsous = new HBox(3);
		hbsous.getChildren().add(btnsous); 
		gridbtn.add(hbsous, 1, 0);
		
		//MLP
		Button btnmlp = new Button("Multiplier");
		HBox hbmlp = new HBox(3); 
		hbmlp.getChildren().add(btnmlp); 
		gridbtn.add(hbmlp, 2, 0); 

		//Div
		Button btndiv = new Button("Diviser"); 
		HBox hbdiv = new HBox(3);
		hbdiv.getChildren().add(btndiv); 
		gridbtn.add(hbdiv, 3, 0);
		
		//resultat
		Label resultat = new Label("Resultat : ");
		gridres.add(resultat, 0, 0);
		Label userRes = new Label();
		gridres.add (userRes, 0,1);
		

		//GridPrincipale
		root.add(gridsaisie, 0, 1);
		root.add(gridbtn, 0, 2);
		root.add(gridres, 0, 3);

		final Text actiontarget = new Text();
		gridsaisie.add(actiontarget, 0, 0); 

		final Text actiontargetbtn = new Text();
		gridbtn.add(actiontargetbtn, 0, 0); 
		
		// Scene
		Scene scene = new Scene(root, 600,400);
		primaryStage.setScene(scene);
		primaryStage.show();
		// MEF de la scene 
		scene.getStylesheets().add(Main_javaFx_etape3.class.getResource("application_javaFx_etape3.css").toExternalForm());
		gridres.setId("mefGridRes");
		gridbtn.setId("mefGridBtn");
		gridsaisie.setId("mefGridSaisie");
		resultat.setId("mefRes");
		userRes.setId("mefuserRes");
		scenetitle.setId("scnTitle");
		//event
		//add
        btnadd.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            public void handle(MouseEvent event) {
            	
            	double nob1, nob2, res;
            	nob1 = Double.parseDouble(tfnb1.getText());
            	nob2 = Double.parseDouble(tfnb2.getText());
            	res = nob1+nob2;
            	event.consume();
            	userRes.setText(Double.toString(res));
            }
    });
      //sous
        btnsous.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            public void handle(MouseEvent event) {
            	
            	double nob1, nob2, res;
            	nob1 = Double.parseDouble(tfnb1.getText());
            	nob2 = Double.parseDouble(tfnb2.getText());
            	res = nob1-nob2;
            	event.consume();
            	userRes.setText(Double.toString(res));
            }
    });
      //mlp
        btnmlp.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            public void handle(MouseEvent event) {
            	
            	double nob1, nob2, res;
            	nob1 = Double.parseDouble(tfnb1.getText());
            	nob2 = Double.parseDouble(tfnb2.getText());
            	res = nob1*nob2;
            	event.consume();
            	userRes.setText(Double.toString(res));
            }
    });
      //div
        btndiv.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            public void handle(MouseEvent event) {
            	//Resultat  addition
            	double nob1, nob2, res;
            	nob1 = Double.parseDouble(tfnb1.getText());
            	nob2 = Double.parseDouble(tfnb2.getText());
            	res = nob1/nob2;
            	event.consume();
            	userRes.setText(Double.toString(res));
            }
    });
	} 
	public static void main(String[] args) {
		launch(args); 
	}
}